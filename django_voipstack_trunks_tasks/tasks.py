from datetime import timedelta

import ldap3
from celery import shared_task
from django.utils.timezone import now
from django_python3_ldap.conf import settings as ldap_settings
from ldap3 import BASE, ALL_ATTRIBUTES, MODIFY_REPLACE
from ldap3.core.exceptions import LDAPNoSuchObjectResult

from django_voipstack_actions.django_voipstack_actions.models import *
from django_voipstack_trunks.django_voipstack_trunks.func import func_add_trunk
from django_voipstack_trunks.django_voipstack_trunks.models import VoipTrunks


@shared_task(name="voipstack_job_trunks")
def voipstack_job_trunks():
    jobs_unprocessed = VoipJob.objects.filter(job_state_id=1, action_id=1)

    for job in jobs_unprocessed:
        if func_add_trunk("sip", job.voipjoboptions_set.get(attribute_id=103).value):
            job.job_state_id = 4
            job.save()

    return


@shared_task(name="voipstack_trunks_sync_ldap")
def voipstack_trunks_sync_ldap():
    # Configure the connection.
    if ldap_settings.LDAP_AUTH_USE_TLS:
        auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
    else:
        auto_bind = ldap3.AUTO_BIND_NO_TLS

    ldap_connection = ldap3.Connection(
        ldap3.Server(
            ldap_settings.LDAP_AUTH_URL,
            allowed_referral_hosts=[("*", True)],
            get_info=ldap3.NONE,
            connect_timeout=ldap_settings.LDAP_AUTH_CONNECT_TIMEOUT,
        ),
        user=ldap_settings.LDAP_AUTH_CONNECTION_USERNAME,
        password=ldap_settings.LDAP_AUTH_CONNECTION_PASSWORD,
        auto_bind=auto_bind,
        raise_exceptions=True,
        receive_timeout=ldap_settings.LDAP_AUTH_RECEIVE_TIMEOUT,
    )

    all_accounts = VoipTrunks.objects.filter(customer__org_tag__isnull=False).all()

    for account in all_accounts:
        tenant_ou = (
            "ou=" + account.customer.org_tag + "," + settings.HOSTING_LDAP_SEARCH_BASE
        )

        cred_dn = (
            "cn=" + account.trunk_name + ",OU=SIP-Trunks,OU=Telephony," + tenant_ou
        )

        print("DN: " + cred_dn)

        try:
            ldap_connection.search(
                search_base=cred_dn,
                search_filter="(objectClass=AsteriskAccount)",
                search_scope=BASE,
                attributes=ALL_ATTRIBUTES,
            )
        except LDAPNoSuchObjectResult:
            # new credentials
            ldap_connection.add(
                dn=cred_dn,
                attributes={
                    "objectClass": [
                        "AsteriskAccount",
                    ],
                    "description": account.trunk_name,
                    "name": account.trunk_name,
                    "AstAccountName": account.trunk_name,
                },
            )

        ldap_connection.modify(
            dn=cred_dn,
            changes={
                "objectClass": [
                    (
                        MODIFY_REPLACE,
                        [
                            "AsteriskAccount",
                            "AsteriskExtension",
                            "AsteriskPjsipAuth",
                            "AsteriskPjsipAor",
                            "AsteriskPjsipEndpoint",
                            "KamailioProxyAuthCred",
                        ],
                    )
                ],
                "description": [(MODIFY_REPLACE, [account.trunk_name])],
                # Asterisk
                "AstAccountName": [(MODIFY_REPLACE, [account.trunk_name])],
                "AstAuthId": [(MODIFY_REPLACE, [account.trunk_name])],
                "AstAuthType": [(MODIFY_REPLACE, ["userpass"])],
                "AstAuthUsername": [(MODIFY_REPLACE, [account.trunk_name])],
                "AstAuthPassword": [(MODIFY_REPLACE, [account.trunk_password])],
                "AstAorId": [(MODIFY_REPLACE, [account.trunk_name])],
                "AstAorMaxContacts": [(MODIFY_REPLACE, ["1"])],
                "AstEndpointId": [(MODIFY_REPLACE, [account.trunk_name])],
                "AstEndpointAors": [(MODIFY_REPLACE, [account.trunk_name])],
                "AstEndpointAuth": [(MODIFY_REPLACE, [account.trunk_name])],
                "AstEndpointTransport": [(MODIFY_REPLACE, ["transport-udp"])],
                "AstEndpointAllowTransfer": [(MODIFY_REPLACE, ["no"])],
                "AstEndpointDirectMedia": [(MODIFY_REPLACE, ["no"])],
                "AstEndpointAllowSubscribe": [(MODIFY_REPLACE, ["no"])],
                "AstEndpointIceSupport": [(MODIFY_REPLACE, ["no"])],
                "AstEndpointSendRpid": [(MODIFY_REPLACE, ["yes"])],
                "AstEndpointContext": [(MODIFY_REPLACE, ["from-internal"])],
                "AstEndpointAggregateMwi": [(MODIFY_REPLACE, ["yes"])],
                "AstEndpointSendPai": [(MODIFY_REPLACE, ["yes"])],
                "AstEndpointDisallow": [(MODIFY_REPLACE, ["all"])],
                "AstEndpointSendDiversion": [(MODIFY_REPLACE, ["yes"])],
                "AstEndpoint100rel": [(MODIFY_REPLACE, ["yes"])],
                "AstEndpointRewriteContact": [(MODIFY_REPLACE, ["yes"])],
                # Kamailio
                "serUID": [(MODIFY_REPLACE, [account.trunk_name])],
                "digestUsername": [(MODIFY_REPLACE, [account.trunk_name])],
                "digestPassword": [(MODIFY_REPLACE, [account.trunk_password])],
                "serFlags": [(MODIFY_REPLACE, ["0"])],
                "digestRealm": [(MODIFY_REPLACE, ["pbx.dolphin-connect.com"])],
            },
        )

        ldap_connection.search(
            search_base=cred_dn,
            search_filter="(objectClass=AsteriskAccount)",
            search_scope=BASE,
            attributes=ALL_ATTRIBUTES,
        )

        ldap_objects = ldap_connection.response

        trunk_attributes = ldap_objects[0]["attributes"]

        # only set defaults
        changes = dict()

        if "AstEndpointInbandProgress" not in trunk_attributes:
            changes["AstEndpointInbandProgress"] = [(MODIFY_REPLACE, ["yes"])]

        if "AstEndpointRtpSymmetric" not in trunk_attributes:
            changes["AstEndpointRtpSymmetric"] = [(MODIFY_REPLACE, ["yes"])]

        if "AstEndpointAllow" not in trunk_attributes:
            changes["AstEndpointAllow"] = [(MODIFY_REPLACE, ["g722;alaw;ulaw"])]

        if "AstEndpointForceRport" not in trunk_attributes:
            changes["AstEndpointForceRport"] = [(MODIFY_REPLACE, ["yes"])]

        if "AstEndpointDtmfMode" not in trunk_attributes:
            changes["AstEndpointDtmfMode"] = [(MODIFY_REPLACE, ["auto"])]

        if len(changes.keys()) > 0:
            ldap_connection.modify(dn=cred_dn, changes=changes)

        # TODO: cleanup / remove old accounts that don't exist in VoIPStack (anymore)

    return


@shared_task(name="voipstack_trunks_auto_suspend")
def voipstack_trunks_auto_suspend():
    for trunk in VoipTrunks.objects.filter(usage_limit=VoipTrunks.NORMAL):
        cdr_count = trunk.voipcdr_set.filter(
            call_start__gte=now() - timedelta(days=90)
        ).count()

        if not cdr_count:
            trunk.usage_limit = VoipTrunks.SUSPENDED
            trunk.save()

    return

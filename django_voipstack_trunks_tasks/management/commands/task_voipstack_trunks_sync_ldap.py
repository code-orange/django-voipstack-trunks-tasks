from django.core.management.base import BaseCommand

from django_voipstack_trunks.django_voipstack_trunks.tasks import (
    voipstack_trunks_sync_ldap,
)


class Command(BaseCommand):
    help = "Run task voipstack_trunks_sync_ldap"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        voipstack_trunks_sync_ldap()

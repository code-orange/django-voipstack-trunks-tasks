from django.core.management.base import BaseCommand

from django_voipstack_trunks_tasks.django_voipstack_trunks_tasks.tasks import (
    voipstack_trunks_auto_suspend,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write(self.help)
        voipstack_trunks_auto_suspend()
